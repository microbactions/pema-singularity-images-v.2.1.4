<p align="center">
  <img src="https://i.paste.pics/870189fadf668a958c8aac83f38e799c.png"  width="300" align="left" >
</p>


# PEMA: 
## a flexible Pipeline for Environmental DNA Metabarcoding Analysis of the 16S/18S rRNA, ITS and COI marker genes 
*PEMA is reposited in* [*Docker Hub*](https://hub.docker.com/r/hariszaf/pema) *as well as in* [*Singularity Hub*](https://singularity-hub.org/collections/2295)

#### A PEMA tutorial can be found [**here**]( https://docs.google.com/presentation/d/1lVH23DPa2NDNBhVvOTRoip8mraw8zfw8VQwbK4vkB1U/edit?fbclid=IwAR14PpWfPtxB8lLBBnoxs7UbG3IJfkArrJBS5f2kRA__kvGDUb8wiJ2Cy_s#slide=id.g57f092f54d_1_21).

#### For any troubles you may have when running PEMA or for any potential improvevments you would like to suggest, please share on the [PEMA Gitter community](https://gitter.im/pema-helpdesk/community).

[![Gitter](https://badges.gitter.im/pema-helpdesk/community.svg)](https://gitter.im/pema-helpdesk/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


-------------


Here is the Singularity version of the **PEMA v.2.1.4** release! 

This release: 

   - is based on a new `pemabase` Docker image ([`pemabase:v.2.1.1`](https://hub.docker.com/layers/hariszaf/pemabase/v.2.1.1/images/sha256-d89052ae0be96c8eaa7f0b5c6b3908d5bac3cfe3c9891a9f26329181352ea29c?context=explore)) which is now completely indepedent from any local resources and the user can build it through the [`Dockerfile.pemabase`](https://github.com/hariszaf/pema/blob/ARMS/pema_docker_image/pemabase/Dockerfile.pemabase) file
   - a new marker gene, the 12S rRNA, is now also supported, by exploiting the [12S Vertebrate Classifier v2.0.0-ref](https://github.com/terrimporter/12SvertebrateClassifier/releases) database
   - for the case of 18S rRNA marker gene, the [PR2 database](https://pr2-database.org/) has been integrated so now the user may select between Silva and PR2  
   - thanks to the [`ncbi-taxonomist`](https://ncbi-taxonomist.readthedocs.io/en/latest/index.html) software, PEMA now provides an extended OTU/ASV table where in last column the NCBI Taxonomy Id 
   for the taxonomic level closer to the species name rank for which there is one, is available   

In addition, in this release: 
   - a `sannity_check` folder with datasets and parameters files for multiple combinations are now available, so tests can be performed to check whether `pema` runs properly after edits.
   - bugs during the analysis of 18S data have been fixed
   - the `perSample` outputs have been fixed 
   - convertion of non ena format raw data for multiple sequencers is supported  




You will be able to get it by running 

```singularity pull pema_v.2.1.4.sif https://gitlab.com/microbactions/pema-singularity-images/-/raw/v.2.1.4/pema_v.2.1.4.sif```

Keep in mind that you alway need to chech the version in the end of the url above, so it matches with the current version.

Sorry for the inconvenience and we hope we will soon have an easier alternative. 

The PEMA team





## License
PEMA is under the GNU GPLv3 license (for 3rd party components separate licenses apply).

## Citation
Haris Zafeiropoulos, Ha Quoc Viet, Katerina Vasileiadou, Antonis Potirakis, Christos Arvanitidis, Pantelis Topalis, Christina Pavloudi, Evangelos Pafilis, PEMA: a flexible Pipeline for Environmental DNA Metabarcoding Analysis of the 16S/18S ribosomal RNA, ITS, and COI marker genes, GigaScience, Volume 9, Issue 3, March 2020, giaa022, https://doi.org/10.1093/gigascience/giaa022
